#!/bin/bash

mkdir -p /share/
mkdir -p /share/html/

CONFIG_FILE=""

# Add apache config for fpm
if [[ $FPM == "on" ]]; then
  echo "Activating fpm proxypass"
  sed -i '13i\\tProxyPassMatch ^/(.*\.php(/.*)?)$ unix:/share/fpm.sock|fcgi://localhost/share/html/' /etc/apache2/sites-enabled/000-default.conf
fi

set -u

if [[ $ENV_TYPE == "dev" ]]; then
  CONFIG_FILE="dev-config.php"
elif [[ $ENV_TYPE == "prod" ]]; then
  CONFIG_FILE="config.php"
fi

OBJECT_SCHEME=""

if [[ $OBJECT_USE_SSL == "true" ]]; then
  echo "using ssl for s3 storage"
  sed -i s/"'use_ssl' => false"/"'use_ssl' => true"/ config/storage.config.php
  OBJECT_SCHEME="https"
elif [[ $OBJECT_USE_SSL == "false" ]]; then
  OBJECT_SCHEME="http"
  echo "not using ssl for s3 storage"
else
  echo "OBJECT_USE_SSL should be true or false; currently set to $OBJECT_USE_SSL"
  exit 1
fi

APPS="notes contacts calendar tasks bruteforcesettings previewgenerator deck files_ebookreader tasks bruteforcesettings forms duplicatefinder"
#APPS="user_usage_report notes contacts calendar quota_warning terms_of_service tasks apporder news deck polls bookmarks bruteforcesettings forms previewgenerator"
DISABLE_APPS="deck polls news support activity survey_client"
#DISABLE_APPS="photos activity support survey_client"


# version_greater A B returns whether A > B
version_greater() {
    [ "$(printf '%s\n' "$@" | sort -t '.' -n -k1,1 -k2,2 -k3,3 -k4,4 | head -n 1)" != "$1" ]
}

# return true if specified directory is empty
directory_empty() {
    [ -z "$(ls -A "$1/")" ]
}

run_as() {
    if [ "$(id -u)" = 0 ]; then
        su -p www-data -s /bin/sh -c "$1"
    else
        sh -c "$1"
    fi
}

echo "DEBUG:"
echo "POSTGRES_USER: $POSTGRES_USER"
echo "$(env)"

if [ -n "${REDIS_HOST+x}" ]; then

  echo "Configuring Redis as session handler"
  {
    echo 'session.save_handler = redis'
    # check if redis host is an unix socket path
    if [ "$(echo "$REDIS_HOST" | cut -c1-1)" = "/" ]; then
      if [ -n "${REDIS_HOST_PASSWORD+x}" ]; then
        echo "session.save_path = \"unix://${REDIS_HOST}?auth=${REDIS_HOST_PASSWORD}\""
      else
        echo "session.save_path = \"unix://${REDIS_HOST}\""
      fi
      # check if redis password has been set
    elif [ -n "${REDIS_HOST_PASSWORD+x}" ]; then
       echo "session.save_path = \"tcp://${REDIS_HOST}:${REDIS_HOST_PORT:=6379}?auth=${REDIS_HOST_PASSWORD}\""
    else
       echo "session.save_path = \"tcp://${REDIS_HOST}:${REDIS_HOST_PORT:=6379}\""
    fi
  } > /usr/local/etc/php/conf.d/redis-session.ini
fi

installed_version="0.0.0.0"
if [ -f /var/www/html/version.php ]; then
  # shellcheck disable=SC2016
  installed_version="$(php -r 'require "/var/www/html/version.php"; echo implode(".", $OC_Version);')"
fi
  # shellcheck disable=SC2016
image_version="$(php -r 'require "/usr/src/nextcloud/version.php"; echo implode(".", $OC_Version);')"

if version_greater "$installed_version" "$image_version"; then
  echo "Can't start Nextcloud because the version of the data ($installed_version) is higher than the docker image version ($image_version) and downgrading is not supported. Are you sure you have pulled the newest image version?"
  exit 1
fi

if version_greater "$image_version" "$installed_version"; then
  echo "Initializing nextcloud $image_version ..."
  if [ "$installed_version" != "0.0.0.0" ]; then
    echo "Upgrading nextcloud from $installed_version ..."
    run_as 'php /var/www/html/occ app:list' | sed -n "/Enabled:/,/Disabled:/p" | cat
  fi
  if [ "$(id -u)" = 0 ]; then
    rsync_options="-rlDog --chown www-data:root"
  else
    rsync_options="-rlD"
  fi
  rsync $rsync_options --delete --exclude-from=/upgrade.exclude /usr/src/nextcloud/ /var/www/html/

  for dir in config data custom_apps themes; do
    if [ ! -d "/var/www/html/$dir" ] || directory_empty "/var/www/html/$dir"; then
      rsync $rsync_options --include "/$dir/" --exclude '/*' /usr/src/nextcloud/ /var/www/html/
    fi
  done

  rsync $rsync_options --include '/version.php' --exclude '/*' /usr/src/nextcloud/ /var/www/html/

  echo "Pulling in user config from /config"

  rsync $rsync_options /config/ /var/www/html/config/
  echo "Initializing finished"

  echo "Fixing up minio client with credentials"

  mkdir -p /var/www/.mc/
  cat << EOF > /var/www/.mc/config.json
{
	"version": "9",
	"hosts": {
		"secret": {
			"url": "YOUR-HOSTSTRING-HERE",
			"accessKey": "YOUR-ACCESS-KEY-HERE",
			"secretKey": "YOUR-SECRET-KEY-HERE",
			"region": "fr-par",
			"api": "S3v2",
			"lookup": "auto"
		}
	}
}
EOF

  chown www-data: /var/www/.mc/config.json

  ls -lathr /var/www/.mc/
  cat /var/www/.mc/config.json
  sed -i s/YOUR-ACCESS-KEY-HERE/$OBJECT_KEY/ /var/www/.mc/config.json
  sed -i s/YOUR-SECRET-KEY-HERE/$OBJECT_SECRET/ /var/www/.mc/config.json
  sed -i s,YOUR-HOSTSTRING-HERE,$OBJECT_SCHEME://$OBJECT_ENDPOINT:$OBJECT_PORT, /var/www/.mc/config.json
  chown -R www-data:root /var/www/.mc/
  cat /var/www/.mc/config.json
#install
  if [ "$installed_version" = "0.0.0.0" ]; then
    echo "New nextcloud instance"

#   if [ -n "${NEXTCLOUD_ADMIN_USER+x}" ] && [ -n "${NEXTCLOUD_ADMIN_PASSWORD+x}" ]; then
   # shellcheck disable=SC2016
    install_options='-n --admin-user "$NEXTCLOUD_ADMIN_USER" --admin-pass "$NEXTCLOUD_ADMIN_PASSWORD"' 
    if [ -n "${NEXTCLOUD_TABLE_PREFIX+x}" ]; then
      # shellcheck disable=SC2016
      install_options=$install_options' --database-table-prefix "$NEXTCLOUD_TABLE_PREFIX"'
    fi
    if [ -n "${NEXTCLOUD_DATA_DIR+x}" ]; then
      # shellcheck disable=SC2016
      install_options=$install_options' --data-dir "$NEXTCLOUD_DATA_DIR"'
    fi

    echo "PostgreSQL database configured"
    install_options=$install_options' --database pgsql --database-name "$POSTGRES_DB" --database-user "$POSTGRES_USER" --database-pass "$POSTGRES_PASSWORD" --database-host "$POSTGRES_HOST:$POSTGRES_PORT"'
	echo "$install_options"

    run_as "mc -C /var/www/.mc/ mb --region $OBJECT_REGION --ignore-existing secret/install" 
    run_as "touch /var/www/html/data/.ocdata"  

    echo "debug: outside if, before ls"           
    sleep $(shuf -i 1-10 -n 1) # random sleep, horrible way to "avoid" race conditions
    secret_exists=`mc -C /var/www/.mc/ ls secret/install/$CONFIG_FILE.enc | wc -l`
    
    #secret_exists=$?
    echo "linecount for ls secret/install/$CONFIG_FILE.enc: $secret_exists"
                
    if [ $secret_exists -eq 0 ]; then # file doesnt exist, so we need to run an install
      echo "debug: inside, config does not exist"
      echo "starting nextcloud installation, remote config not found"
      run_as "mc -C /var/www/.mc/ cp -q config/config.php secret/install/$CONFIG_FILE.enc" # copy an empty file in place to effectively lock this and prevent and install race condition
      touch /var/www/html/config/CAN_INSTALL
      chown www-data: /var/www/html/config/CAN_INSTALL                
      max_retries=5
      try=0
      until run_as "php /var/www/html/occ maintenance:install $install_options" || [ "$try" -gt "$max_retries" ]
      do
        echo "retrying install..."
        try=$((try+1))
        sleep 10s
      done

      run_as "php /var/www/html/occ config:system:get instanceid"
      
      run_as "openssl aes-256-cbc -a -salt -in config/config.php -out $CONFIG_FILE.enc -k $INSTALL_KEY"
      run_as "mc -C /var/www/.mc/ cp -q $CONFIG_FILE.enc secret/install/$CONFIG_FILE.enc"
      echo "debug: end of inside if"
    elif [ $secret_exists -ne 0 ]; then # remote file does exist
      echo "debug: inside, config exists"
      echo "remote config exists, fetching and decrypting"
      run_as "mc -C /var/www/.mc/ cp -q secret/install/$CONFIG_FILE.enc $CONFIG_FILE.enc"
      max_retries=20
      run_as "openssl aes-256-cbc -d -a -in $CONFIG_FILE.enc -out config/config.php -k $INSTALL_KEY"
      try=0                  
#      until run_as "mc -C /var/www/.mc/ cp --region $OBJECT_REGION -q secret/install/config.php.enc config.php.enc" || [ "$try" -gt "$max_retries" ]
      until [ -s /var/www/html/config/config.php ] || [ "$try" -gt "$max_retries" ]
      do
        echo "trying config fetch"
        run_as "mc -C /var/www/.mc/ cp -q secret/install/$CONFIG_FILE.enc $CONFIG_FILE.enc"
        run_as "openssl aes-256-cbc -d -a -in $CONFIG_FILE.enc -out config/config.php -k $INSTALL_KEY"                      
        try=$((try+1))
        sleep 10s
      done

    rm config/storage.config.php
    CONFIG_VERSION=$(php -r 'require "/var/www/html/config/config.php"; echo $CONFIG["version"];')
    CONTAINER_VERSION=$(php -r 'require "/var/www/html/version.php"; echo $OC_Version[0].".".$OC_Version[1].".".$OC_Version[2].".".$OC_Version[3];')
    echo "sed -i s/$CONFIG_VERSION/$CONTAINER_VERSION/g /var/www/html/config/config.php"
    sed -i s/$CONFIG_VERSION/$CONTAINER_VERSION/g /var/www/html/config/config.php

    else 
      echo "debug: secret exit code neither 0 nor 1, set to $secret_exists"
    fi
    instanceid=$(php -r 'require "/var/www/html/config/config.php"; echo $CONFIG["instanceid"];')
#    ./occ config:system:set instanceid --value zzz
    run_as "php /var/www/html/occ config:system:get instanceid"                

    git clone --depth "1" --branch "v18.0.4" https://github.com/mwalbeck/nextcloud-breeze-dark.git /var/www/html/apps/breezedark

    echo "installing apps"
    for app in $APPS ; do
      echo "installing $app"
      run_as "php /var/www/html/occ app:install -n -vvv $app" 
    done

    for app in $DISABLE_APPS ; do
      echo "installing $app"
      run_as "php /var/www/html/occ app:disable -n -vvv $app" 
    done

   if [ "$NEXTCLOUD_TRUSTED_DOMAINS" != "" ] && [ "$NEXTCLOUD_TRUSTED_DOMAINS" != "localhost" ]; then
      run_as "./occ config:system:set overwriteprotocol --value=https"
    fi

    run_as "./occ background:webcron"

    if [ -n "${NEXTCLOUD_TRUSTED_DOMAINS+x}" ]; then
      echo "setting trusted domains..."
      NC_TRUSTED_DOMAIN_IDX=1
      for DOMAIN in $NEXTCLOUD_TRUSTED_DOMAINS ; do
        DOMAIN=$(echo "$DOMAIN" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')
        run_as "php /var/www/html/occ config:system:set trusted_domains $NC_TRUSTED_DOMAIN_IDX --value=$DOMAIN"
        NC_TRUSTED_DOMAIN_IDX=$(($NC_TRUSTED_DOMAIN_IDX+1))
      done
    fi

    echo "Fixing favicon"
    cp /var/www/html/favicon.ico /var/www/html/core/img/favicon.ico
  fi
fi

rsync -avz /var/www/html /share/

chown -R www-data:root /share
chown -R www-data:root /var/www/html

exec "$@"
