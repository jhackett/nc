provider "scaleway" {
  zone            = "fr-par-1"
  region          = "fr-par"
}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "scaleway_object_bucket" "nextcloud" {
    name = "nextcloudcoop-b"
    acl = "private"
}

resource "scaleway_k8s_cluster_beta" "k8s" {
  name = "k8s-b"
  version = "1.18"
  cni = "calico"
  auto_upgrade {
  	enable = true
  	maintenance_window_day = "any"
  	maintenance_window_start_hour = "2"
  }
}


resource "scaleway_k8s_pool_beta" "k8s" {
  cluster_id = scaleway_k8s_cluster_beta.k8s.id
  name = "k8s-b"
  node_type = "DEV1-M"
  size = 1
  min_size = 1
  max_size = 10
  autoscaling = true
  autohealing = true
}

provider "kubernetes" {
  host  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].host
  token  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].token
  cluster_ca_certificate = base64decode(
    scaleway_k8s_cluster_beta.k8s.kubeconfig[0].cluster_ca_certificate
  )
}

output "kubeconfig" {
  value = scaleway_k8s_cluster_beta.k8s.kubeconfig
}

resource "tls_private_key" "tls" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.tls.private_key_pem
  email_address   = "john@firstbit.uk"
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.reg.account_key_pem
  common_name               = "cloud.firstbit.uk"
#  subject_alternative_names = ["cloud.firstbit.uk"]

  dns_challenge {
    provider = "gandiv5"
  }
}

#resource "kubernetes_secret" "nextcloud-tls" {
#  metadata {
#    name = "nextcloud-tls"
#  }

#  data = {
#    "tls.crt" = acme_certificate.certificate.certificate_pem
#    "tls.key" = acme_certificate.certificate.private_key_pem
#  }

#  type = "kubernetes.io/tls"
#}


#resource "kubernetes_secret" "tls" {
#  metadata {
#    name = "tls"
#  }

#  data = {
#    "tls.crt" = acme_certificate.certificate.certificate_pem
#    "tls.key" = acme_certificate.certificate.private_key_pem
#  }

#  type = "kubernetes.io/tls"
#}

resource "random_password" "postgres" {
  length = 50
  special = true
}

#resource "scaleway_rdb_instance_beta" "postgres" {
#    name = "test-rdb"
#    node_type = "db-dev-s"
#    engine = "PostgreSQL-9.6"
#    is_ha_cluster = false
#    disable_backup = true
#    user_name = "nextcloud"
#   password = random_password.postgres.result
#}

provider "helm" {
  kubernetes {
    host     = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].host
    token = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].token
#    cluster_ca_certificate = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].cluster_ca_certificate
    insecure = true
  }
}


resource "helm_release" "http_ingress" {
  name       = "http"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "nginx-ingress"
  namespace  = "default"
  timeout = 900
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "prometheus"
  namespace  = "default"
  timeout = 900
}
