x 2020-03-24 2020-03-23 Install and enable nextcloud apps during build or startup
x 2020-04-17 Nextcloud cluster handling in installer pri:A
x 2020-04-12 2020-03-27 Kubernetes config for deployments and services of app, postgres, redis pri:A
x 2020-04-12 2020-03-29 Derive kubernetes secrets from terraform https://www.terraform.io/docs/providers/kubernetes/r/secret.html pri:A
x 2020-04-27 2020-04-23 client_max_body_size for ingress controller pri:A
x 2020-04-27 2020-04-13 S3 caching with minio caching + gateway pri:A
(A) 2020-05-13 Business plan
(A) 2020-04-17 Helm for environment building
(A) 2020-05-17 Chasquid container
(A) 2020-05-17 Dovecot container
(A) 2020-05-13 Sidecar containers for apps
(B) 2020-05-13 Sidecar containers for metrics
x 2020-05-13 2020-04-12 Terraform dns to avoid irritating slip-ups.. pri:B
(B) 2020-03-26 Market research!
(B) 2020-03-26 Scope out £5/month offering
(B) 2020-03-24 Scope out £10/year offering 
(B) Explore HostBill as a solution for billing and user management +users
(B) 2020-03-26 Helm chart tests\n(nextcloudcmd uploads files successfully?)
x 2020-03-24 Explore user management strategies +users\nOutcome: HostBill uses nextcloud's user API, and Dovecot can auth against oauth2 IDPs pri:A
x 2020-03-21 Move minio to container resource +tech pri:B
x 2020-03-21 Move postgres to azure resource pri:C
x 2020-03-23 Secret injection needs moving from build time to runtime\nSolution: use "getenv()" in php configs pri:C
x 2020-03-24 2020-03-20 Model {my,our}cloudcoop pricing with Azure
(C) 2020-03-21 Dovecot auth against nextcloud? \nhttps://docs.nextcloud.com/server/14/admin_manual/configuration_server/oauth2.html\nhttps://wiki.dovecot.org/PasswordDatabase/oauth2
(D) 2020-03-27 Ethical consumer advertising
(J) 2020-03-27 Fair tax mark accreditation
x 2020-05-13 2020-03-29 Autoconfig rainloop https://github.com/pierre-alain-b/rainloop-nextcloud pri:D
