locals {
  postgres_user = "pgadmin"
  postgres_nextcloud_user = "nextcloud_admin"
  postgres_db = "nextcloud"
}


provider "scaleway" {
  zone            = "fr-par-1"
  region          = "fr-par"
}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "scaleway_object_bucket" "nextcloud" {
    name = "nextcloudcoop-a"
    acl = "private"
}

resource "scaleway_k8s_cluster_beta" "k8s" {
  name = "k8s"
  version = "1.18"
  cni = "calico"
  auto_upgrade {
  	enable = true
  	maintenance_window_day = "any"
  	maintenance_window_start_hour = "2"
  }
}

resource "null_resource" "crd_install" {
  provisioner "local-exec" {
    command = "kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.crds.yaml"
  }
}

resource "scaleway_k8s_pool_beta" "k8s" {
  cluster_id = scaleway_k8s_cluster_beta.k8s.id
  name = "k8s"
  node_type = "DEV1-L"
  size = 1
  min_size = 1
  max_size = 10
  autoscaling = true
  autohealing = true
  container_runtime = "docker"
}

provider "kubernetes" {
  host  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].host
  token  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].token
  cluster_ca_certificate = base64decode(
    scaleway_k8s_cluster_beta.k8s.kubeconfig[0].cluster_ca_certificate
  )
}

output "kubeconfig" {
  value = scaleway_k8s_cluster_beta.k8s.kubeconfig
}

resource "tls_private_key" "tls" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.tls.private_key_pem
  email_address   = "john@firstbit.uk"
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.reg.account_key_pem
  common_name               = "cloud.firstbit.uk"
#  subject_alternative_names = ["cloud.firstbit.uk"]

  dns_challenge {
    provider = "gandiv5"
  }
}

resource "kubernetes_config_map" "params" {
  metadata {
    name = "nextcloud"
  }

  data = {
    postgres_user = local.postgres_user
    postgres_host = "${scaleway_rdb_instance_beta.postgres.endpoint_ip}:${scaleway_rdb_instance_beta.postgres.endpoint_port}"
    postgres_db = local.postgres_db
    env_type = "prod"
  }
}

resource "kubernetes_secret" "params" {
  metadata {
    name = "nextcloud"
  }

  data = {
    postgres_password = random_password.nextcloud_postgres.result
  }

  type = "kubernetes.io/opaque"
}


resource "random_password" "postgres" {
  length = 50
  special = true
}

resource "random_password" "nextcloud_postgres" {
  length = 50
  special = true
}

resource "scaleway_rdb_instance_beta" "postgres" {
  name = "nextcloud"
  node_type = "db-dev-s"
  engine = "PostgreSQL-9.6"
  is_ha_cluster = false
  disable_backup = false
  user_name = local.postgres_user
  password = random_password.postgres.result
}

provider "helm" {
  kubernetes {
    host = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].host
    token = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].token
#    cluster_ca_certificate = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].cluster_ca_certificate
    insecure = true
  }
}


resource "helm_release" "cert_manager" {
  name       = "tls"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  namespace  = "default"
  timeout = 900
  set {
    name = "installCRDs"
    value = "true"
  }
}


resource "helm_release" "http_ingress" {
  name       = "http"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "nginx-ingress"
  namespace  = "default"
  timeout = 900
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  repository = "https://kubernetes-charts.storage.googleapis.com"
  chart      = "prometheus"
  namespace  = "default"
  timeout = 900
}

provider "postgresql" {
  host            = scaleway_rdb_instance_beta.postgres.endpoint_ip
  port            = scaleway_rdb_instance_beta.postgres.endpoint_port
#  database        = "rdb"
  username        = local.postgres_user
  password        = random_password.postgres.result
  superuser       = false
  sslmode         = "require"
  connect_timeout = 15
}

#resource "postgresql_role" "nextcloud_admin" {
#  name             = local.postgres_nextcloud_user
#  create_database  = true
#  create_role      = true
#  login            = true
#  connection_limit = 100
#  password         = random_password.nextcloud_postgres.result
#}

#resource postgresql_grant "nextcloud_admin" {
#  database    = postgresql_database.nextcloud.name
#  role        = postgresql_role.nextcloud_admin.name
#  schema      = "public"
#  object_type = "table"
#  privileges  = ["ALL"]
#}

#resource "postgresql_database" "nextcloud" {
#  name              = local.postgres_db
#  owner             = postgresql_role.nextcloud_admin.name
#  template          = "template0"
#  lc_collate        = "C"
#  connection_limit  = 100
#  allow_connections = true
#}
