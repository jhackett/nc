provider "helm" {
  kubernetes {
    config_path = "/home/j/.kube/config"
  }
}

data "helm_repository" "bitnami_stable" {
  name = "stable"
  url  = "https://charts.bitnami.com/bitnami"
}

data "helm_repository" "default_stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}


#resource "helm_release" "redis" {
#  name  = "redis"
#  chart = "bitnami_stable/redis"
#  repository = data.helm_repository.bitnami_stable.metadata[0].name
  
#  set {
#    name  = "metrics.enabled"
#    value = "true"
#  }

#  set {
#    name  = "redisPort"
#    value = "6379"
#  }

#  set {
#  	name = "sentinels.enabled"
#  	value = "true"
#  }
#}

resource "helm_release" "postgres" {
  name  = "postgres"
  chart = "bitnami_stable/postgresql-ha"
  repository = data.helm_repository.bitnami_stable.metadata[0].name
  
  set {
    name  = "persistence.enabled"
    value = "true"
  }

  set {
    name  = "persistence.size"
    value = "25Gi"
  }
}

resource "helm_release" "nginx-ingress" {
  name  = "nextcloud-ingress"
  chart = "stable/nginx-ingress"
  repository = data.helm_repository.default_stable.metadata[0].name

  set {
  	name = "controller.metrics.enabled"
  	value = "true"
  }
}

resource "helm_release" "nextcloud" {
  name  = "nextcloud"
  chart = "stable/nextcloud"
  repository = data.helm_repository.default_stable.metadata[0].name

  set {
    name  = "ingress.enabled"
    value = "true"
  }

  set {
    name  = "nginx.enabled"
    value = "true"
  }

  set {
  	name = "internalDatabase.enabled"
  	value = "false"
  }

  set {
  	name = "externalDatabase.enabled"
  	value = "true"
  }

  set {
    name = "redis.enabled"
    value = "true"
  }

  set {
  	name = "cronjob.enabled"
  	value = true
  }

  set {
  	name = "service.type" 
  	value = "LoadBalancer"
  }

  set {
  	name = "livenessProbe.enabled"
  	value = "true"
  }

  set {
  	name = "livenessProbe.initialDelaySeconds"
  	value = "180"
  }

  set {
  	name = "nextcloud.configs.s3.config.php"
  	value = <<EOT
      <?php
      $CONFIG = array (
        'objectstore' => array(
          'class' => '\\OC\\Files\\ObjectStore\\S3',
          'arguments' => array(
            'bucket'     => 'my-bucket',
            'autocreate' => true,
            'key'        => 'xxx',
            'secret'     => 'xxx',
            'region'     => 'us-east-1',
            'use_ssl'    => true
          )
        )
      );
  	EOT
  }
}

resource "helm_release" "prometheus" {
  name  = "prometheus"
  chart = "prometheus"
  repository = data.helm_repository.default_stable.metadata[0].name
}
