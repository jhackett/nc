remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "nextcloudcoop-tf-state"
    key = "${path_relative_to_include()}/terraform.tfstate"
#    encrypt = true
    region = "fr-par"
    access_key = get_env("BUCKET")
    secret_key = get_env("BUCKET")
    endpoint = "https://s3.fr-par.scw.cloud"
  }
}