minikube:
	minikube start
	eval $(minikube docker-env)

up: 
	terraform apply -auto-approve
	make env
	docker-compose up -d

down: 
	docker-compose down --remove-orphans
	docker volume prune -f

full:
	docker build --no-cache -t nextcloudcoop . 
	docker-compose down --remove-orphans
	docker volume prune -f 
	docker-compose up -d 

logs:
	docker-compose logs 
	docker-compose exec --user www-data app-1 php occ log:tail

logs-2:
	docker-compose logs 
	docker-compose exec --user www-data app-2 php occ log:tail
fast: 
	docker-compose down --remove-orphans
	docker volume prune -f 
	docker-compose up -d 

build: 
	docker build --no-cache -t nextcloudcoop . 

configs: full
	docker-compose exec app cat /var/www/html/config/storage.config.php

show-credential:
	terraform show -json | jq '.values.root_module.resources[] | select(.address == "azurerm_storage_account.nextcloudcoop") | .values.primary_access_key'

env:
	echo OBJECT_SECRET=`terraform show -json | jq -r '.values.root_module.resources[] | select(.address == "azurerm_storage_account.nextcloudcoop") | .values.primary_access_key'` > .env
	echo OBJECT_KEY=`terraform show -json | jq -r '.values.root_module.resources[] | select(.address == "azurerm_storage_account.nextcloudcoop") | .values.name'` >> .env
	echo MINIO_SECRET_KEY=`terraform show -json | jq -r '.values.root_module.resources[] | select(.address == "azurerm_storage_account.nextcloudcoop") | .values.primary_access_key'` >> .env
	echo MINIO_ACCESS_KEY=`terraform show -json | jq -r '.values.root_module.resources[] | select(.address == "azurerm_storage_account.nextcloudcoop") | .values.name'` >> .env
	echo POSTGRES_DB=nextcloud >> .env
	echo POSTGRES_USER=nextcloud >> .env
	echo POSTGRES_PASSWORD=password >> .env