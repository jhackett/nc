FROM nextcloud:21-apache

RUN a2enmod proxy_fcgi
RUN apt-get update && apt-get -y install jq wget git sudo && rm -rf /var/lib/apt/lists/*
RUN mkdir -p config && \
mkdir -p custom_apps && \
touch /var/www/html/config/CAN_INSTALL && \
touch /var/www/html/config/config.php && \
chown -R www-data:www-data /var/www/html/config  && \
chown -R www-data:www-data /var/www/html/custom_apps  && \
chown www-data:root /var/www/html/config/config.php && \
wget https://dl.min.io/client/mc/release/linux-amd64/mc -P /bin/ && \
wget https://github.com/jhass/nextcloud-keeweb/archive/refs/tags/v0.6.5.tar.gz -P /tmp/packages/ && \
chmod +x /bin/mc && \
mkdir -p /var/www/.mc && \
chown -R www-data: /var/www/.mc && \
mkdir -p /config/
COPY ./assets/favicon.ico /var/www/html/favicon.ico
#COPY ./mc /bin/mc
#RUN chmod +x /bin/mc
COPY --chown=www-data ./config/storage.config.php /config/storage.config.php
COPY --chown=www-data ./config/config.php /config/config.php
#COPY --chown=www-data ./config/minio.json /var/www/.mc/config.json
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
# ProxyPassMatch ^/(.*\.php(/.*)?)$ unix:/var/run/fpm.sock|fcgi://var/www/html
RUN echo "Redirect 301 /.well-known/carddav /remote.php/dav\n"\
  "Redirect 301 /.well-known/caldav /remote.php/dav"\
  >> /etc/apache2/sites-available/000-default.conf

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]
