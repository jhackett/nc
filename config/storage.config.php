<?php
if (getenv('OBJECT_BUCKETNAME')) {
	$CONFIG = array(
	  'objectstore' => array(
	        'class' => '\\OC\\Files\\ObjectStore\\S3',
	        'arguments' => array(
	                'bucket' => getenv('OBJECT_BUCKETNAME'),
	                'autocreate' => true,
	                'region' => getenv('OBJECT_REGION'),
	                'key'    => getenv('OBJECT_KEY'),
	                'secret' => getenv('OBJECT_SECRET'),
	                'hostname' => getenv('OBJECT_ENDPOINT'),
	                'port' => getenv('OBJECT_PORT'),
	                'use_ssl' => false,
	//                'region' => 'optional',
	                // required for some non Amazon S3 implementations
	                'use_path_style'=>true
	        ),
	  ),
	  'config_is_read_only' => true,
	  'memcache.local' => '\OC\Memcache\APCu',
	  'memcache.distributed' => '\OC\Memcache\Redis',
	  'memcache.locking' => '\OC\Memcache\Redis',
	  'redis' => [
	       'host' => getenv('REDIS_HOST'),
	       'port' => 6379,
	  ],
	);
}
