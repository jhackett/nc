helm install postgres bitnami_stable/postgresql-ha -f postgres-values.yaml
helm install ingress stable/nginx-ingress -f ingress-values.yaml
helm install nextcloud stable/nextcloud -f nextcloud-values.yaml
helm install prometheus prometheus -f prometheus-values.yaml