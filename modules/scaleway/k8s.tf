provider "helm" {}

data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

resource "helm_release" "ingress-nginx" {
  name       = "ingress-nginx"
  repository = data.helm_repository.stable.metadata[0].name
  chart      = "nginx-ingress"
  namespace  = kubernetes_namespace.nginx.metadata[0].name

  timeout = 900
}

data "helm_repository" "cert-manager" {
  name = "jetstack"
  url  = "https://charts.jetstack.io"
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  repository = data.helm_repository.cert-manager.metadata[0].name
  chart      = "cert-manager"
  namespace  = kubernetes_namespace.cert-manager.metadata[0].name

  timeout = 900
}
