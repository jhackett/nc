provider "scaleway" {
  zone            = "fr-par-1"
  region          = "fr-par"
}

provider "acme" {
  server_url = "https://acme-v02.api.letsencrypt.org/directory"
}

resource "scaleway_object_bucket" "nextcloud" {
    name = "nextcloudcoop-{{}}"
    acl = "private"
}

resource "scaleway_k8s_cluster_beta" "k8s" {
  name = "k8s"
  version = "1.16.1"
  cni = "calico"
  default_pool {
    node_type = "DEV1-M"
    size = 2
  }
}

provider "kubernetes" {
  host  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].host
  token  = scaleway_k8s_cluster_beta.k8s.kubeconfig[0].token
  cluster_ca_certificate = base64decode(
    scaleway_k8s_cluster_beta.k8s.kubeconfig[0].cluster_ca_certificate
  )
}

#output "kubeconfig" {
#  value = scaleway_k8s_cluster_beta.k8s.kubeconfig
#}

resource "tls_private_key" "tls" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "acme_registration" "reg" {
  account_key_pem = tls_private_key.tls.private_key_pem
  email_address   = "john@firstbit.uk"
}

resource "acme_certificate" "certificate" {
  account_key_pem           = acme_registration.reg.account_key_pem
  common_name               = "cloud.firstbit.uk"
#  subject_alternative_names = ["cloud.firstbit.uk"]

  dns_challenge {
    provider = "gandiv5"
  }
}

resource "kubernetes_secret" "nextcloud-tls" {
  metadata {
    name = "nextcloud-tls"
  }

  data = {
    "tls.crt" = acme_certificate.certificate.certificate_pem
    "tls.key" = acme_certificate.certificate.private_key_pem
  }

  type = "kubernetes.io/tls"
}


resource "kubernetes_secret" "tls" {
  metadata {
    name = "tls"
  }

  data = {
    "tls.crt" = acme_certificate.certificate.certificate_pem
    "tls.key" = acme_certificate.certificate.private_key_pem
  }

  type = "kubernetes.io/tls"
}

#resource "tls_self_signed_cert" "tls" {
#  key_algorithm   = "ECDSA"
#  private_key_pem = tls_private_key.tls.private_key_pem
#  
#  subject {
#    common_name  = "cloud.firstbit.uk"
#    organization = "firstbit uk"
#  }
#
#  validity_period_hours = 12
#
#  allowed_uses = [
#    "key_encipherment",
#    "digital_signature",
#    "server_auth",
#  ]
#}


#resource "random_password" "postgres" {
#  length = 50
#  special = true
#}

#resource "scaleway_rdb_instance_beta" "postgres" {
#    name = "test-rdb"
#    node_type = "db-dev-s"
#    engine = "PostgreSQL-9.6"
#    is_ha_cluster = false
#    disable_backup = true
#    user_name = "nextcloud"
#    password = random_password.postgres.result
#}

